IMAGE_URL=usvc/ci-node

# override whatever you want in here
-include ./Makefile.properties

DATE_TIMESTAMP=$$(date +'%Y')$$(date +'%m')$$(date +'%d')

build:
	$(MAKE) build_base
	$(MAKE) build_docker
	$(MAKE) build_gitlab
build_base:
	docker build \
		--tag $(IMAGE_URL):latest \
		--target base \
		.
build_docker:
	docker build \
		--tag $(IMAGE_URL):docker-latest \
		--target docker \
		.
build_gitlab:
	docker build \
		--tag $(IMAGE_URL):gitlab-latest \
		--target gitlab \
		.

ci.export:
	$(MAKE) ci.export_base
	$(MAKE) ci.export_docker
	$(MAKE) ci.export_gitlab
ci.export_base:
	mkdir -p ./.export
	docker save --output ./.export/base.tar $(IMAGE_URL):latest
ci.export_docker:
	mkdir -p ./.export
	docker save --output ./.export/docker.tar $(IMAGE_URL):docker-latest
ci.export_gitlab:
	mkdir -p ./.export
	docker save --output ./.export/gitlab.tar $(IMAGE_URL):gitlab-latest

ci.import:
	-$(MAKE) ci.import_base
	-$(MAKE) ci.import_docker
	-$(MAKE) ci.import_gitlab
ci.import_base:
	docker load --input ./.export/base.tar
ci.import_docker:
	docker load --input ./.export/docker.tar
ci.import_gitlab:
	docker load --input ./.export/gitlab.tar

test:
	$(MAKE) test_base
	$(MAKE) test_docker
	$(MAKE) test_gitlab
test_base: build_base
	container-structure-test test \
	 	--verbosity debug \
	 	--image $(IMAGE_URL):latest \
		--config ./shared/tests/base.yml
	$(MAKE) test_from TAG=latest FROM_URL="https://gitlab.com/usvc/images/ci/base/raw/master/shared/tests/base.yml"
test_docker: build_docker
	container-structure-test test \
	 	--verbosity debug \
	 	--image $(IMAGE_URL):docker-latest \
		--config ./shared/tests/base.yml
	$(MAKE) test_from TAG=docker-latest FROM_URL="https://gitlab.com/usvc/images/ci/docker/raw/master/shared/tests/base.yml"
test_gitlab: build_gitlab
	container-structure-test test \
	 	--verbosity debug \
	 	--image $(IMAGE_URL):gitlab-latest \
		--config ./shared/tests/base.yml
	container-structure-test test \
	 	--verbosity debug \
	 	--image $(IMAGE_URL):gitlab-latest \
		--config ./shared/tests/gitlab.yml
test_from:
	@if [ "${TAG}" = "" ]; then \
		printf -- "\n\n\033[1m> you need to specify a TAG environment variable\033[0m\n\n"; \
		exit 1; \
	elif [ "${FROM_URL}" = "" ]; then \
		printf -- "\n\n\033[1m> you need to specify a FROM_URL environment variable\033[0m\n\n"; \
		exit 1; \
	fi
	curl -Lo ./shared/tests/from.yml ${FROM_URL}
	container-structure-test test \
	 	--verbosity debug \
	 	--image $(IMAGE_URL):${TAG} \
		--config ./shared/tests/from.yml
	rm -rf ./shared/tests/from.yml

version_alpine:
	mkdir -p ./.version
	docker run --entrypoint=cat \
		$(IMAGE_URL):${TAG} /etc/alpine-release \
		> ./.version/alpine
version_docker:
	mkdir -p ./.version
	docker run \
		--entrypoint=docker \
		--volume /var/run/docker.sock:/var/run/docker.sock \
		$(IMAGE_URL):${TAG} version \
			--format '{{ .Client.Version }}' \
		> ./.version/docker
version_node:
	mkdir -p ./.version
	docker run \
		--entrypoint=node \
		$(IMAGE_URL):${TAG} -v \
		> ./.version/node

publish:
	$(MAKE) publish_base
	$(MAKE) publish_docker
	$(MAKE) publish_gitlab
publish_base: build_base
	mkdir -p ./.version
	docker push $(IMAGE_URL):latest
	# usvc/ci-node:YYYYMMDD
	$(MAKE) utils.tag_and_push FROM=latest TO=$(DATE_TIMESTAMP)
	# usvc/ci-node:alpine-<alpine_version>
	$(MAKE) version_alpine TAG=latest
	$(MAKE) utils.tag_and_push FROM=latest TO=alpine-$$(cat ./.version/alpine)
	# usvc/ci-node:<node_version>
	$(MAKE) version_node TAG=latest
	$(MAKE) utils.tag_and_push FROM=latest TO=$$(cat ./.version/node)
publish_docker: build_docker
	mkdir -p ./.version
	docker push $(IMAGE_URL):docker-latest
	# usvc/ci-node:YYYYMMDD
	$(MAKE) utils.tag_and_push FROM=docker-latest TO=docker-$(DATE_TIMESTAMP)
	# usvc/ci-node:docker-alpine-<alpine_version>
	$(MAKE) version_alpine TAG=docker-latest
	$(MAKE) utils.tag_and_push FROM=docker-latest TO=docker-alpine-$$(cat ./.version/alpine)
	# usvc/ci-node:docker-<docker_version>
	$(MAKE) version_docker TAG=docker-latest
	$(MAKE) utils.tag_and_push FROM=docker-latest TO=docker-$$(cat ./.version/docker)
	# usvc/ci-node:docker-go-<go_version>
	$(MAKE) version_node TAG=docker-latest
	$(MAKE) utils.tag_and_push FROM=docker-latest TO=docker-node-$$(cat ./.version/node)
publish_gitlab: build_gitlab
	mkdir -p ./.version
	docker push $(IMAGE_URL):gitlab-latest
	# usvc/ci-node:YYYYMMDD
	$(MAKE) utils.tag_and_push FROM=gitlab-latest TO=gitlab-$(DATE_TIMESTAMP)
	# usvc/ci-node:gitlab-alpine-<alpine_version>
	$(MAKE) version_alpine TAG=gitlab-latest
	$(MAKE) utils.tag_and_push FROM=gitlab-latest TO=gitlab-alpine-$$(cat ./.version/alpine)
	# usvc/ci-node:gitlab-<docker_version>
	$(MAKE) version_docker TAG=docker-latest
	$(MAKE) utils.tag_and_push FROM=gitlab-latest TO=gitlab-$$(cat ./.version/docker)
	# usvc/ci-node:gitlab-go-<go_version>
	$(MAKE) version_node TAG=gitlab-latest
	$(MAKE) utils.tag_and_push FROM=gitlab-latest TO=gitlab-node-$$(cat ./.version/node)

utils.tag_and_push:
	docker tag $(IMAGE_URL):${FROM} $(IMAGE_URL):${TO}
	docker push $(IMAGE_URL):${TO}
